#!/bin/bash
FILE="*.adoc"

fswatch -o $FILE | while read f; do
  clear
  asciidoctor -r asciidoctor-diagram $FILE
done